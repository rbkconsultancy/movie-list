# MovieList

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.2.


This is the front end project code for the now playing movie list project.

## Getting Started
Follow the below instructions to serve movie-list application locally for development and testing purposes. 

## Application Operating Instruction

 Nos | Instructions | 
:-----------: | :-----------: |
1 | On Navigating to `http://localhost:4200/` will load initial now playing movie list which has ratings 3 or greater by default. |
2 | Genre list will be automatically filtered with respect to the movie list displayed. |
3 | When user changes the rating value in slider which results in refreshing movie list and in turn refreshes genre list. |
4 | If user checks or un-checks genre will refresh movie list accordingly. |
5 | Movie list view will show title, poster image, genre names with respect to genre_ids and overview. |


### Prerequisites
#### Frontend
Download and Install one of the following software.

 Webstorm | IntelliJ IDEA | Microsoft Visual Code 
 :-----------: | :-----------: | :-----------:
 [Mac](https://www.jetbrains.com/webstorm/download/#section=mac) /  [Windows](https://www.jetbrains.com/webstorm/download/#section=windows)  /  [Linux](https://www.jetbrains.com/webstorm/download/#section=linus) | [Mac](https://www.jetbrains.com/idea/download/#section=mac)  /  [Windows](https://www.jetbrains.com/idea/download/#section=windows)  /  [Linux](https://www.jetbrains.com/idea/download/#section=linux) | [Download link for Windows / Linux / Mac](https://code.visualstudio.com/download)


## Download frontend codebase
Frontend source code can be downloaded by clicking this [link](https://bitbucket.org/rbkconsultancy/movie-list/src/master/)  

(or) 
```
Step 1 : git clone git@bitbucket.org:rbkconsultancy/movie-list.git

Step 2 : npm install
```


## Running the SSWB front end application

```
ng serve    (or)    ng s
```


## Build and Test
Test with live reload
```
ng test --watch=true
```

One off test run
```
ng test 
```

Generate test coverage report
```
npm run test
```

Build the app locally
```
npm run build
```

## Authors

* **Ravindra Bharathi** - * Initial creation of the Now Paying Movie List*

## License

This is a public project and wholly owned by *Ravindra Bharathi*

##Useful Plugins
Useful Webstorm / IntelliJ Plugins.

Karma, ESLint, Git, File Watchers, Docker Integration, CSS Support, SonarQube Community Plugin, SonarLint, Scaff Angular, Angular Templates, Angular2 Typescript LIve Templates, .ignore


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

#Screenshots

![Code Coverage Report](/src/assets/images/readme/code-coverage-report.png)

![Now Playing Movie List](/src/assets/images/readme/1-now-playing-movie-list-..jpeg)

![No Data Found](/src/assets/images/readme/2-no-data-found-..jpeg)
