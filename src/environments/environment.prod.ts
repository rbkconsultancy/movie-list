export const environment = {
  production: true,
  CI: {
    API_URL: 'https://localhost:5001/ci',
    API_PATH: 'api',
  },
  TEST: {
    API_URL: 'https://localhost:5001/test',
    API_PATH: 'api',
  },
  UAT: {
    API_URL: 'https://localhost:5001',
    API_PATH: 'uat/api',
  },};
