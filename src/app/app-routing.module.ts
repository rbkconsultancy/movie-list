import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MovieListingComponent} from './components/movie-listing/movie-listing.component';

const routes: Routes = [
  // { path: '', pathMatch: 'full', redirectTo: 'movie-list'},
  // { path: 'movie-list', component: MovieListingComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
