import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import {NavLeftComponent} from './components/nav-left/nav-left.component';
import {
  MatCheckboxModule, MatChip, MatChipList,
  MatDrawer,
  MatDrawerContainer,
  MatDrawerContent,
  MatIcon,
  MatNavList,
  MatSlider,
  MatToolbar
} from '@angular/material';
import {MovieListingComponent} from './components/movie-listing/movie-listing.component';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {GenreListComponent} from './components';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule, MatCheckboxModule, BrowserAnimationsModule
      ],
      declarations: [
        AppComponent, GenreListComponent, NavLeftComponent, MovieListingComponent, MatToolbar, MatNavList, MatDrawerContainer,
        MatSlider, MatDrawer, MatIcon, MatDrawerContent, MatChip, MatChipList
      ],
      providers:  [ HttpClient, HttpHandler]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
