import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { LayoutModule } from '@angular/cdk/layout';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatCheckbox, MatCheckboxModule, MatChip, MatChipList,
  MatIconModule,
  MatListModule,
  MatSidenavModule, MatSlider,
  MatToolbarModule,
} from '@angular/material';

import {HttpClient, HttpHandler} from '@angular/common/http';
import {MovieListingComponent, GenreListComponent} from '../';
import {of} from 'rxjs';
import {MockGenreListComponentData} from './test_data/mock-genre-list-data';
import {GenreService, MoviesService} from '../../services';


class MockGenreService {
  getData() {
    return of(MockGenreListComponentData.responseData);
  }
  getRefreshGenreList () {
    return of(true);
  }
  refreshGenreList() {
    return MockGenreListComponentData.refreshGenreListData;
  }
  setSelectedGenreData() {
    return MockGenreListComponentData.valueChangedGenre;
  }
}


class MockMovieService {
  listedMovieGenres = [53, 878, 28, 27, 18, 28, 80, 28, 53, 16, 28, 36, 16, 12, 16, 28, 14, 10751, 10752, 10749];
}


describe('GenreListComponent', () => {
  let component: GenreListComponent;
  let fixture: ComponentFixture<GenreListComponent>;


  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GenreListComponent,  MovieListingComponent, MatSlider, MatChip, MatChipList],
      imports: [
        NoopAnimationsModule,
        LayoutModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatListModule,
        MatSidenavModule,
        MatToolbarModule,
      ],
      providers: [HttpClient, HttpHandler]
    }).compileComponents();

    TestBed.overrideComponent(
      GenreListComponent,
      {
        set: {
          providers: [
            {provide: GenreService, useClass: MockGenreService},
            {provide: MoviesService, useClass: MockMovieService}
          ]
        }
      }
    );

    fixture = TestBed.createComponent(GenreListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.ngOnInit();
  });

  it('[1] should create', () => {
    expect(component).toBeTruthy();
  });

  it('[2] should push selected genre from the genre list', () => {
    component.valueChanged(MockGenreListComponentData.valueChangedGenre, MockGenreListComponentData.valueChangedEventTrue);
    expect(component.genreIdArray.length).toEqual(1);
  });

  it('[3] should delete the selected genre', () => {
    component.valueChanged(MockGenreListComponentData.valueChangedGenre, MockGenreListComponentData.valueChangedEventFalse);
    expect(component.genreIdArray.length).toEqual(0);
  });
});
