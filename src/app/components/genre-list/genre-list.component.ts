import {Component, OnInit, OnDestroy} from '@angular/core';
import {BreakpointObserver} from '@angular/cdk/layout';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {GenreService, MoviesService} from '../../services';

@Component({
  selector: 'app-genre-list',
  templateUrl: './genre-list.component.html',
  styleUrls: ['./genre-list.component.scss']
})
export class GenreListComponent implements OnInit, OnDestroy {

  private ngUnsubscribe = new Subject();
  allGenre: Array<any>;
  genreIdArray: Array<number>;
  errorMessage: string;

  constructor(private genreService: GenreService,
              private movieService: MoviesService,
              private breakpointObserver: BreakpointObserver) {

    // Purpose: This subscription is refresh genre list based on now playing list as a result of user changed rating / voting slider.
    this.genreService.getRefreshGenreList()
      .subscribe(genre => {
        this.allGenre = this.genreService.refreshGenreList(this.movieService.listedMovieGenres);
      });
  }

  ngOnInit() {
    // Get initial load of genre list.
    this.getGenres();
  }

  /*
  Purpose:  To retrieve all genre details from database and filter the genre data based on the current listed movie genre ids.
            Piping takeUntil operator will be used during destroy event to unSubscribe all subscriptions.
  */
  getGenres = () => {
    this.genreIdArray = [];
    this.genreService.getData()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(response => {
        if (!!response && !!response.genres) {
          this.genreService.unFilteredGenres = response.genres;
          this.allGenre = this.genreService.refreshGenreList(this.movieService.listedMovieGenres);
        }
      }, error => {
        this.errorMessage = 'Cannot retrieve genre list.';
      }).add(() => {
        this.unSubscribeAllSubscriptions();
      }
    );
  }

  /*
  Purpose:  To add or remove genre id to / from an array which will be set to a behaviour subject variable.
            The behaviour subject will be subscribed from movie listing component which will be used to filter now playing movie list.
  */
  valueChanged(genre, event) {
    if (!!event && event.checked) {
      this.genreIdArray.push(genre.id);
    } else {
      this.genreIdArray.splice(this.genreIdArray.indexOf(genre.id), 1);
    }

    this.genreService.setSelectedGenreData(this.genreIdArray);
  }

  /*
  Purpose:  Unsubscribe all subscriptions using takeUntil operator while subscribing response data.
  */
  unSubscribeAllSubscriptions = () => {
    console.log('Genre Component: All subscription(s) unSubscribed');
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  };

  /*
  Purpose:  Unsubscribe all subscriptions.
  */
  ngOnDestroy() {
    this.unSubscribeAllSubscriptions();
  }

}
