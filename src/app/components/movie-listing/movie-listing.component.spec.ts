import { LayoutModule } from '@angular/cdk/layout';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatButtonModule,
  MatCardModule, MatChip, MatChipList,
  MatGridListModule,
  MatIconModule,
  MatMenuModule, MatSlider, MatToolbar,
} from '@angular/material';

import {GenreService, MoviesService} from '../../services';
import { MovieListingComponent } from './movie-listing.component';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {of} from 'rxjs';
import {MockMovieListingComponentData} from './test_data/mock-movie-list-data';

class MockMoviesService {
  getData() {
    return of(MockMovieListingComponentData.responseData);
  }

  filterMoviesByRating () {
    return MockMovieListingComponentData.filterMoviesByRatingReturnData;
  }

  getActiveMovieGenres() {
    return MockMovieListingComponentData.getActiveMovieGenresReturnData;
  }

}

class MockGenreService {

  setRefreshGenreList() {
    return MockMovieListingComponentData.listedMovieGenres;
  }

  getRefreshGenreList() {
    return of(true);
  }

  getSelectedGenreData() {
    return of(true);
  }

  retrieveGenreNameList() {
    return MockMovieListingComponentData.retrieveGenreNameList;
  }

  // retrieveGenreNameList() {}

}

describe('MovieListingComponent', () => {
  let component: MovieListingComponent;
  let fixture: ComponentFixture<MovieListingComponent>;

  beforeEach(( () => {
    TestBed.configureTestingModule({
      declarations: [MovieListingComponent, MatSlider, MatToolbar, MatChip, MatChipList],
      imports: [
        NoopAnimationsModule,
        LayoutModule,
        MatButtonModule,
        MatCardModule,
        MatGridListModule,
        MatIconModule,
        MatMenuModule,
      ],
      providers: [ HttpClient, HttpHandler]
    }).compileComponents();

    TestBed.overrideComponent(
      MovieListingComponent,
      {
        set: {
          providers: [
            {provide: MoviesService, useClass: MockMoviesService},
            {provide: GenreService, useClass: MockGenreService}
            ]
        }
      }
    );

    fixture = TestBed.createComponent(MovieListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.ngOnInit();
  }));


  it('[1] should compile', () => {
    expect(component).toBeTruthy();
  });

  it('[2] should return now playing movies list which has rating 3 or greater', () => {
    component.getListOfMovies();
    expect(component.allMovies.length).toEqual(20);
  });

});
