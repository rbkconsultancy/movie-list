import { Component, OnInit, OnDestroy } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { GenreService, MoviesService } from '../../services';

@Component({
  selector: 'app-movie-listing',
  templateUrl: './movie-listing.component.html',
  styleUrls: ['./movie-listing.component.scss']
})
export class MovieListingComponent implements OnInit, OnDestroy  {
  private ngUnsubscribe = new Subject();
  allMovies: Array<any>;
  genreIdArray: Array<number>;
  rating: number;
  totalMovies: number;
  errorMessage: string;

  constructor(private movieService: MoviesService, private genreService: GenreService) {

    // The below service method will be subscribed when an user checks or unchecks a genre from the genre list menu.
    this.genreService.getSelectedGenreData()
      .subscribe(genre => {
        this.genreIdArray = genre;
        this.updateNowPlayingMovieList(this.rating, genre);
      });
  }

  ngOnInit() {
    this.rating = 3.0;
    this.genreIdArray = [];
    this.getListOfMovies();
  }

  getListOfMovies = () => {
    this.movieService.getData()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(response => {

        if (!!response && !!response.results) {
          const data = response.results;
          this.movieService.unFilteredMovies = data;

          // Filter now playing list by default rating set which is 3
          this.allMovies = this.movieService.filterMoviesByRating(data, this.rating);

          // Valid list of genre data from the above filtered now playing movie list.
          this.movieService.getActiveMovieGenres(this.allMovies);

          // By setting the valid list of genre data to the behaviour subject would invoke
          // the genre list component to refresh genre data
          this.genreService.setRefreshGenreList(this.movieService.listedMovieGenres);

          // Get the genre names from the genre_ids from the now playing movie list.
          // the genre names array will then be displayed in the UI
          this.genreService.retrieveGenreNameList(this.allMovies);

          this.totalMovies = this.allMovies.length;
        }

        this.unSubscribeAllSubscriptions();
      }, error => {
        this.errorMessage = 'Cannot retrieve movies list.';
      });
  }

  updateNowPlayingMovieList = (rating: number = 3.0, selectedGenres: Array<any> = []) => {

    // Update now playing movie list respect to user's opted genre from the genre list menu
    this.rating = rating;
    this.allMovies = this.movieService.filterMoviesByRating(this.movieService.unFilteredMovies, rating);
    this.movieService.getActiveMovieGenres(this.allMovies);

    if (selectedGenres.length > 0) {
      this.allMovies = this.movieService.filterMoviesByGenre(this.allMovies, selectedGenres);
    }
    this.genreService.setRefreshGenreList(this.movieService.listedMovieGenres);

    this.totalMovies = this.allMovies.length;
  }

  unSubscribeAllSubscriptions = () => {
    /*
    Purpose:  Using behaviour subject we can unsubscribe all subscriptions using takeUntil operator while subscribing.
    */
    console.log('Movie Listing Component: All subscription(s) unSubscribed');
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  ngOnDestroy() {
    this.unSubscribeAllSubscriptions();
  }

}
