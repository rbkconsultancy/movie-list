import { LayoutModule } from '@angular/cdk/layout';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatChip, MatChipList,
         MatIconModule, MatListModule, MatSidenavModule, MatSlider, MatToolbarModule,
        } from '@angular/material';

import { NavLeftComponent, MovieListingComponent, GenreListComponent } from '../';

describe('NavLeftComponent', () => {
  let component: NavLeftComponent;
  let fixture: ComponentFixture<NavLeftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavLeftComponent, GenreListComponent,  MovieListingComponent, MatSlider, MatChip, MatChipList],
      imports: [
        NoopAnimationsModule,
        LayoutModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatListModule,
        MatSidenavModule,
        MatToolbarModule,
      ],
      providers: [HttpClient, HttpHandler]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
