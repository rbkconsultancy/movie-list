import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MovieListingComponent} from './components/movie-listing/movie-listing.component';
import { MatGridListModule, MatCardModule, MatMenuModule, MatCheckboxModule, MatChipsModule,
  MatIconModule, MatButtonModule, MatToolbarModule, MatSliderModule, MatSidenavModule, MatListModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import {HttpClientModule} from '@angular/common/http';
import {FlexLayoutModule} from '@angular/flex-layout';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavLeftComponent } from './components/nav-left/nav-left.component';
import { GenreListComponent } from './components/genre-list/genre-list.component';

@NgModule({
  declarations: [
    AppComponent,
    MovieListingComponent,
    NavLeftComponent,
    GenreListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    HttpClientModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatCheckboxModule,
    MatChipsModule,
    MatIconModule,
    MatButtonModule,
    MatToolbarModule,
    MatSliderModule,
    LayoutModule,
    NgbModule.forRoot(),
    MatSidenavModule,
    MatListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
