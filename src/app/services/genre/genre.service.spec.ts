import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import {GenreService} from './genre.service';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {MockGenreServiceData} from './test_data/mock-genre-service-data';
import {Observable, of} from 'rxjs';


describe('GenreService', () => {

  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let genreService: GenreService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GenreService]
    });

    // Inject the http, test controller, and service-under-test as they will be referenced by each test.
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    genreService = TestBed.get(GenreService);
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should be created', () => {
    const service: GenreService = TestBed.get(GenreService);
    expect(service).toBeTruthy();
  });


  // GET DATA
  describe('[1] #geData', () => {
    let expectedData: any[];

    beforeEach(() => {
      genreService = TestBed.get(GenreService);
      expectedData = MockGenreServiceData.GenreData;
      genreService.setSelectedGenreData([MockGenreServiceData.NowPlayingGenreIdsData]);
      genreService.setRefreshGenreList([{'id': 28, 'name': 'Action'}]);
    });

    it('[1A]: should return expected role data (called once)', () => {
      const endPoint = 'https://api.themoviedb.org/3/genre/movie/list?api_key=b2e11d6607ef5baef00a698cd0cdf346&language=en-US';
      genreService.getData().subscribe(
        data => expect(data).toEqual(expectedData, 'should return expected genre data'),
        fail
      );

      // GenreService should have made one request to GET call using the endpoint URL
      const req = httpTestingController.expectOne(endPoint);
      expect(req.request.method).toEqual('GET');
      req.flush(expectedData);
    });

    it('[1B]: should return refreshed genre list with respect to play now movie list', () => {
      genreService.unFilteredGenres = MockGenreServiceData.unFilteredGenres;
      expect(genreService.unFilteredGenres.length).toEqual(19);

      const data = genreService.refreshGenreList(MockGenreServiceData.listedMovieGenres);
      expect(data.length).toEqual(11);
    });

    it('[1C]: should add genre name list data to movie list data', () => {
      const oneMovie = [MockGenreServiceData.oneMovie];
      genreService.unFilteredGenres = MockGenreServiceData.unFilteredGenres;
      // expect(oneMovie['genre_names']).toBeUndefined();

      const data = genreService.retrieveGenreNameList(oneMovie);
      expect(data[0].genre_names).toBeDefined();
    });
  });
});
