import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
// import { environment } from '../../environments/environment';
import {Observable, Subject, throwError as observableThrowError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GenreService {

  endPoint: string;
  unFilteredGenres: Array<any>;
  private genreIdSubject = new Subject<Array<any>>();
  private refreshGenreListSubject = new Subject<Array<any>>();

  constructor(private http: HttpClient) {
    this.endPoint = 'https://api.themoviedb.org/3/genre/movie/list?api_key=b2e11d6607ef5baef00a698cd0cdf346&language=en-US';
  }

  getData(): Observable<any> {
    return this.http.get(this.endPoint).pipe(
      map(res => res || []),
      catchError(this.handleError)
    );
  }

  setSelectedGenreData = (idArr) => {
    this.genreIdSubject.next(idArr);
  }

  getSelectedGenreData(): Observable<any> {
    return this.genreIdSubject.asObservable();
  }

  setRefreshGenreList = (idArr) => {
    this.refreshGenreListSubject.next(idArr);
  }

  getRefreshGenreList(): Observable<any> {
    return this.refreshGenreListSubject.asObservable();
  }

  refreshGenreList = (listedMovieGenres) => {
    let data;
    if (listedMovieGenres.length > 0 ) {
      data = this.unFilteredGenres.filter(genre => listedMovieGenres.includes(genre.id));
    }
    return data;
  }

  retrieveGenreNameList = (data) => {
    const originalGenreList = this.unFilteredGenres;
    data.filter(function(dt) {
      const genNameArr = [];
      const currentMovie = originalGenreList.filter((gen) => dt.genre_ids.includes(gen.id));
      currentMovie.map(e => genNameArr.push(e.name));
      dt.genre_names = genNameArr || [];
    });
    return data;
  }


  private handleError = (error: HttpErrorResponse) => {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      observableThrowError(error.statusText);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      observableThrowError(error.statusText);
    }
    // return an observable with a user-facing error message
    return observableThrowError('Something else broken; please try again later.');
  }

}
