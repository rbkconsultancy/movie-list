import { TestBed } from '@angular/core/testing';

import { MoviesService } from './movies.service';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {MockMovieServiceData} from './test_data/mock-movie-service-data';

describe('MoviesService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let moviesService: MoviesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [MoviesService ]
    });

    // Inject the http, test controller, and service-under-test as they will be referenced by each test.
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    moviesService = TestBed.get(MoviesService);
  });


  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });


  it('should be created', () => {
    const service: MoviesService = TestBed.get(MoviesService);
    expect(service).toBeTruthy();
  });


  // GET DATA
  describe('[1] #geData', () => {
    let expectedData: any[];

    beforeEach(() => {
      moviesService = TestBed.get(MoviesService);
      expectedData = MockMovieServiceData.MovieListData;
      moviesService.endPoint = 'https://api.themoviedb.org/3/movie/now_playing?api_key=b2e11d6607ef5baef00a698cd0cdf346&language=en-US&page=1;';
    });

    it('[1A]: should return expected now playing movie list', () => {
      const endPoint = 'https://api.themoviedb.org/3/movie/now_playing?api_key=b2e11d6607ef5baef00a698cd0cdf346&language=en-US&page=1;';

      moviesService.getData().subscribe(
        data => expect(data).toEqual(expectedData, 'should return expected now playing movie data'),
        fail
      );

      // MoviesService should have made one request to GET call using the endpoint URL
      const req = httpTestingController.expectOne(endPoint);
      expect(req.request.method).toEqual('GET');
      req.flush(expectedData);
    });

    it('[1B]: should populate list of genre ids with respect to now playing movie list', () => {
      // moviesService.unFilteredMovies = ;
      moviesService.getActiveMovieGenres(MockMovieServiceData.unFilteredMovies);
      expect(moviesService.listedMovieGenres).toBeDefined();
    });

    it('[1C]: should filter now playing movie list respect to rating', () => {
      // moviesService.unFilteredMovies = ;
      const data = MockMovieServiceData.unFilteredMovies;
      let rtnData;
      rtnData = moviesService.filterMoviesByRating(data, 3);
      expect(rtnData.length).toEqual(20);
      rtnData = moviesService.filterMoviesByRating(data, 7.5);
      expect(rtnData.length).toEqual(3);
    });

    it('[1D]: should filter now playing movie list respect to genre checkbox selected', () => {
      // moviesService.unFilteredMovies = ;
      const data = MockMovieServiceData.unFilteredMovies;
      let rtnData;
      rtnData = moviesService.filterMoviesByGenre(data, [14]);
      expect(rtnData.length).toEqual(7);
      rtnData = moviesService.filterMoviesByGenre(data, [878]);
      expect(rtnData.length).toEqual(4);
    });

  });

  
});
