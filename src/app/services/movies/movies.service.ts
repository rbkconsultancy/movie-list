import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError, map, takeUntil} from 'rxjs/operators';
// import { environment } from '../../environments/environment';
import {Observable, Subject, throwError as observableThrowError} from 'rxjs';
import {GenreService} from '../genre/genre.service';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  endPoint: string;
  unFilteredMovies: Array<any>;
  listedMovieGenres: Array<number>;

  constructor(private http: HttpClient, private genreService: GenreService) {
    // NOTE 1: To display error message, update the endPoint with invalid url

    // NOTE 2:  API_URL can be placed in environment.ts and it can be retrieved using an endPointService.
    //          For demo purpose the api url is hard coded here.
    this.endPoint = 'https://api.themoviedb.org/3/movie/now_playing?api_key=b2e11d6607ef5baef00a698cd0cdf346&language=en-US&page=1;';
    this.listedMovieGenres = [];
  }

  getData(): Observable<any> {
    return this.http.get(this.endPoint).pipe(
      map(res => res || []),
      catchError(this.handleError)
    );
  }

  getActiveMovieGenres = (data) => {
    // Retrieving active genre id with respect to now playing movie list.
    this.listedMovieGenres = [];
    data.filter(movie => movie.genre_ids.some(genre => this.listedMovieGenres.push(genre)));

    // To get unique array items, using spread operator and Set.
    // this.listedMovieGenres = [...new Set([...this.listedMovieGenres])];
  }


  filterMoviesByRating = (data, rating) => {
    data = data.filter(movie => movie.vote_average >= rating);
    return data;
  }

  filterMoviesByGenre = (data, genreArray: Array<any> = []) => {
    if (genreArray.length > 0) {
      return data.filter(movie => movie.genre_ids.some(genre => genreArray.includes(genre)));
    }
  }


  private handleError = (error: HttpErrorResponse) => {
    if (error.hasOwnProperty('error')) {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        observableThrowError(error.statusText);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: ${error.error}`);
        observableThrowError(error.statusText);
      }
    }
    // return an observable with a user-facing error message
    return observableThrowError('Something else broken; please try again later.');
  }
}
